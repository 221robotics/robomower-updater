#!/bin/bash

if [ ! -d "/root/.ssh" ]; then
    ln -s /data/.ssh /root/.ssh
fi

chmod -R 700 /root/.ssh

echo "Beginning mower git update process..."
echo -e "Updating @ `date '+%Y-%m-%d @ %H:%M:%S'`\n"

/usr/bin/wget -q --spider http://google.com

if [ $? -eq 0 ]; then
    echo "Online. Updating git repos..."

    # pulling in public key of git server
  	while true
  	do
    		if [ -s /root/.ssh/known_hosts ]
    		then
      			echo "SSH keys acquired!"
      			break
    		else
      			echo "Scanning SSH host for keys..."
      			ssh-keyscan bitbucket.org > /root/.ssh/known_hosts
      			sleep 1
    		fi
  	done

    # pull git repos
    if [ ! -d "/data/robomower-backend" ]; then
        cd /data
        /usr/bin/git clone git@bitbucket.org:221robotics/robomower-backend.git
    else
        cd /data/robomower-backend
        /usr/bin/git pull origin master
    fi

    if [ ! -d "/data/robomower-web" ]; then
        cd /data
        /usr/bin/git clone git@bitbucket.org:221robotics/robomower-web.git
    else
        cd /data/robomower-web
        /usr/bin/git pull origin master
    fi
else
    echo -e "Offline. Skipping git pull.\n"
fi

echo "Sleeping for 5 mins..."
sleep 300
